<?php

namespace Tests\Unit\Helper;

use App\Helper\DateHandler;
use Tests\TestCase;

class DateHandlerTest extends TestCase
{
    /**
     * @var DateHandler
     */
    private $dateHandler;

    public function setUp()
    {
        parent::setUp();
        $this->dateHandler = new DateHandler();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetDateRange()
    {
        $dateRange = $this->dateHandler->getDateRange(2);

        $this->assertArrayHasKey("today", $dateRange);
        $this->assertArrayHasKey("past", $dateRange);
        $this->assertArrayHasKey("forecast", $dateRange);
    }

    public function testBuildDateRange()
    {
        $dateRange = $this->dateHandler->getDateRange(2);
        $this->assertEquals(2, count($dateRange["past"]));
        $this->assertEquals(2, count($dateRange["forecast"]));
    }
}

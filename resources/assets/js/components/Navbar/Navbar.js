import React, { Component } from 'react';
import SearchBox from '../SearchBox';
import './Navbar.css'

class Navbar extends Component {

    sendQueryStringToParent(query) {
        this.props.searchSubmit(query);
    }

    render() {
        return (
            <nav>
                <ul className="navbar-container">
                    <li className="navbar-list-item">
                        <SearchBox
                            searchSubmit={this.sendQueryStringToParent.bind(this)} />
                    </li>
                </ul>
            </nav>
        );
    }
}

export default Navbar;
<?php

namespace App\Helper;

final class DateHandler
{
    /**
     * Return organized dates by today, forecast and past
     *
     * @param $range
     * @return array
     */
    public function getDateRange($range)
    {
        $dateRange = [];
        $today = new \DateTime(date('Y-m-d'), new \DateTimeZone('UTC'));
        $todayTimestamp = $today->getTimestamp();

        $dateRange['today'] = $todayTimestamp;
        $dateRange['forecast'] = $this->buildDateRange($range, true);
        $dateRange['past'] = $this->buildDateRange($range, false);

        return $dateRange;

    }

    /**
     * Build date range based by range (today-number_of_days, today+number_of_days)
     *
     * @param $range
     * @param $isForecast
     * @return array
     */
    private function buildDateRange($range, $isForecast)
    {
        $dateRange = [];
        $signal = ($isForecast ? "+" : "-");
        for ($i = 1; $i <= $range; $i++) {
            $date = new \DateTime(date("Y-m-d"));
            $date->modify($signal . $i . ' days');
            $dateRange[$i] = $date->getTimestamp();
        }

        return $dateRange;
    }
}
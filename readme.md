## About this project

Weather application using Laravel (5.6) as backend API and React as frontend. 
The weather conditions are retrieved from DarkSky API.

The application shows the weather based on client location (you must allow the browser to use your current location).

The search box was implemented using Google Places API.

## How to setup

This project uses docker-compose alongside with docker.

#### To bring the environment up:

`git clone <repo> weather`

`cd weather`

`docker-compose up  -d`

To the next steps, make sure you are inside the docker container:

`./shell` (to access docker container)

#### Install node and dependencies on alpine linux:

`sudo apk --no-cache update`

`sudo apk --no-cache add autoconf`

`sudo apk add --update nodejs nodejs-npm`

`sudo apk --no-cache add g++ make bash zlib-dev libpng-dev`

`sudo rm -rf /var/cache/apk/*`

#### Install composer dependencies
`composer install`

#### Install node dependencies

`npm install`

#### Configure credentials
Copy .env.example file to .env and add your DarkSky API key to:
DARKSKY_API_KEY=`<your key>`

#### Start React Application

`npm run watch`

Access http://127.0.0.1 and play around

#### PHP Unit tests

To run unit tests:

`./shell`

`vendor/bin/phpunit -c phpunit.xml`

#### Developed by
Leandro Freire |
leandro.freire08@gmail.com
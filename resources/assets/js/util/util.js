const blue = ["clear-night", "snow", "sleet", "partly-cloudy-night"];
const yellow = ["clear-day", "partly-cloudy-day"];
const gray = ["rain", "wind", "fog", "cloudy"];

const CLASS_BLUE = 'wi-blue';
const CLASS_YELLOW = 'wi-yellow';
const CLASS_GRAY = 'wi-gray';

export const getIconClassName = function(weatherId) {
    const hasWeatherId = (idArray, id) => (idArray.indexOf(id) > -1);
    if (hasWeatherId(blue, weatherId)) return CLASS_BLUE;
    else if (hasWeatherId(yellow, weatherId)) return CLASS_YELLOW;
    else if (hasWeatherId(gray, weatherId)) return CLASS_GRAY;
    else return CLASS_BLUE;
}
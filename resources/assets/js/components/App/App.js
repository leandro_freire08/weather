import React, { Component } from 'react';
import Navbar from '../Navbar';
import TodayComponent from '../TodayComponent';
import axios from 'axios';
import './App.css'
import ListComponent from "../ListComponent/ListComponent";

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            queryString: '',
            latLng: {},
            navbarData: {},
            todayComponentData: {},
            pastListComponentData: [],
            forecastListComponentData: [],
            loading: true
        };
    }

    onSearchSubmit(latLng) {
        this.setState({
            latLng: latLng
        }, this.notifyStateChange)
    }

    componentDidMount() {
        // 1. navigator.geolocation will provide coordinates
        const geolocation = navigator.geolocation;
        if (geolocation) {
            const permissionGranted = (position) => {
                let latLng = {lat: position.coords.latitude, lng: position.coords.longitude};
                console.log("LAT: " + latLng.lat + " LNG: " + latLng.lng);
                this.setState({
                    latLng: latLng
                }, this.notifyStateChange);
            };

            const permissionDenied = () => {
                console.log('Permission Denied');
            };

            geolocation.getCurrentPosition(permissionGranted, permissionDenied);
        } else {
            console.log('GeoLocation not supported...Update the browser fella');
        }
    }

    notifyStateChange() {
        const hasLatLng = (this.state.latLng.hasOwnProperty('lat') && this.state.latLng.hasOwnProperty('lng'));
        console.log("Has LatLng: " + hasLatLng);

        if (hasLatLng) {
            this.fetchWeatherForecast(hasLatLng).then(forecastData => {
                // Extract component specific data...
                const todayComponentData = this.extractDataForTodayComponent(forecastData.today);
                const pastListComponentData = this.extractDataForListComponent(forecastData, 'past');
                const forecastListComponentData = this.extractDataForListComponent(forecastData, 'forecast');

                this.setState({
                    todayComponentData,
                    pastListComponentData,
                    forecastListComponentData,
                    loading: false
                })

            }).catch(error => {
                console.log('Error:', error);
            });
        }
    }

    fetchWeatherForecast(hasLatLng) {
        const BASE_URL = 'http://127.0.0.1/api/weather';
        const queryParams = (hasLatLng) ? `lat=${this.state.latLng.lat}&lon=${this.state.latLng.lng}` : '';

        const url = `${BASE_URL}?${queryParams}`;

        return axios.get(url).then(response => {
            return response.data;
        }).catch(error => {
            console.log('Error:', error);
        })
    }

    extractDataForTodayComponent(forecastData) {
        const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

        const todayForecast = forecastData.currently;

        const time = new Date(todayForecast.time * 1000);
        const day = this.getDay(time);
        const date = `${monthNames[time.getMonth()]} ${time.getDate()}, ${time.getFullYear()}`;
        const weather = todayForecast.icon;

        const description = todayForecast.summary;

        let mainTemperature = todayForecast.temperature;
        mainTemperature = Math.round(mainTemperature);


        return {
            day,
            date,
            description,
            mainTemperature,
            weather
        }
    }

    extractDataForListComponent(forecastData, key) {
        const listComponentData = [];
        const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

        forecastData[key].forEach(forecast => {
            let item = {};
            let time = new Date(forecast.currently.time * 1000);
            item.day = this.getDay(time);
            item.date = `${monthNames[time.getMonth()]} ${time.getDate()}, ${time.getFullYear()}`;
            item.weather = forecast.currently.icon;
            item.description = forecast.currently.summary;
            item.temperature = Math.round(forecast.currently.temperature);

            listComponentData.push(item);
        });

        return {
            listComponentData
        }
    }

    getDay(time) {
        const daysNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday ", "Friday", "Saturday"];
        let d = new Date(time);
        d.setTime( d.getTime() + d.getTimezoneOffset()*60*1000 ); // Set datetime based on timezone
        return daysNames[(d.getDay())];
    }

    render() {

        let content;

        if (this.state.loading) {
            content = <img src="./images/loader.gif" className="loader"/>
        } else {
            content = <div>
                <div className="app-today">
                    <TodayComponent data={this.state.todayComponentData}/>
                </div>
                <div className="app-list-graph">
                    <div className="app-past">
                        <span className="title">Past 30 days</span>
                        <ListComponent data={this.state.pastListComponentData}/>
                    </div>
                    <div className="app-forecast">
                        <span className="title">Forecast 30 days</span>
                        <ListComponent data={this.state.forecastListComponentData}/>
                    </div>
                </div>
            </div>
        }

        return (
            <div className="app-container">
                <div className="app-nav">
                    <Navbar
                        searchSubmit={this.onSearchSubmit.bind(this)}
                    />
                </div>
                {content}
            </div>
        );
    }
}

export default App;
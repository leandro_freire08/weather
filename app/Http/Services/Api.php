<?php

namespace App\Http\Services;

use Naughtonium\LaravelDarkSky\DarkSky;
use GuzzleHttp\Promise;

class Api extends DarkSky
{
    const TODAY = "today";
    const FORECAST = "forecast";
    const PAST = "past";

    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * Api constructor.
     */
    public function __construct(\GuzzleHttp\Client $client)
    {
        parent::__construct();

        $this->client = $client;
    }

    /**
     * Perform multiple requests to DarkSky API
     *
     * @param $dateRange
     * @return array
     */
    public function getByDateRange($dateRange)
    {
        $promises = $this->createPromises($dateRange);

        $results = Promise\settle($promises)->wait();

        $responses = [];
        $currentDate = new \DateTime(date("Y-m-d"));
        $currentTimestamp = $currentDate->getTimestamp();
        foreach ($results as $key => $result) {
            if (array_key_exists("value", $result)) {

                if ($key < $currentTimestamp && $key != self::TODAY) {
                    $responses[self::PAST][] = json_decode($results[$key]["value"]->getBody());
                } else if ($key > $currentTimestamp) {
                    $responses[self::FORECAST][] = json_decode($results[$key]["value"]->getBody());
                }

                $responses[self::TODAY] = json_decode($results[self::TODAY]["value"]->getBody());
            }
        }

        // Invert array order from greater to lower in past dates
        $responses[self::PAST] = array_reverse($responses[self::PAST]);

        return $responses;
    }

    /**
     * Create Guzzle Async promises
     *
     * @param $dateRange
     * @return array
     */
    private function createPromises($dateRange)
    {
        $url = $this->endpoint . '/' . $this->lat . ',' . $this->lon;

        $promises = [];
        foreach ($dateRange as $key => $value) {
            if ($key == self::TODAY) {
                $promises[self::TODAY] = $this->client->getAsync($url . "," . $value, [
                    'query' => [
                        'exclude' => 'minutely,hourly,daily,alerts,flags',
                        'units' => 'auto'
                    ],
                ]);
            } else {
                foreach ($dateRange[$key] as $day => $timestamp) {
                    $promises[$timestamp] = $this->client->getAsync($url . "," . $timestamp, [
                        'query' => [
                            'exclude' => 'minutely,hourly,daily,alerts,flags',
                            'units' => 'auto'
                        ],
                    ]);
                }
            }
        }

        return $promises;
    }
}
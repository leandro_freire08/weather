// import React, { Component } from 'react';
// import './SearchBox.css';
// class SearchBox extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             query: ''
//         };
//     }
//
//     handleQueryStringChange(e){
//         this.setState({
//             query: e.target.value
//         })
//     };
//
//     handleSearch(e) {
//         e.preventDefault();
//         console.log('Fetch weather data for:', this.state.query);
//         this.props.searchSubmit(this.state.query);
//     };
//
//     render() {
//         return (
//             <div className="form-container">
//                 <form onSubmit={this.handleSearch.bind(this)}>
//                     <input
//                         type="text"
//                         value={this.state.query}
//                         name="searchBox"
//                         id="searchBox"
//                         placeholder="Enter City or Zipcode"
//                         onChange={this.handleQueryStringChange.bind(this)} />
//                     <span className="search-button fa fa-search" onClick={this.handleSearch.bind(this)} />
//                 </form>
//             </div>
//         );
//     }
// }
//
// export default SearchBox;

import React, { Component } from 'react';
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete'
import './SearchBox.css';

class SearchBox extends Component {
    constructor(props) {
        super(props);
        this.state = { address: '' }
    }

    handleChange(address) {
        this.setState({ address: address })
    }

    handleSelect(address) {
        let self = this;
        geocodeByAddress(address)
            .then(results => getLatLng(results[0]))
            .then(function(latLng) {
                self.props.searchSubmit(latLng);
            })
            .catch(error => console.error('Error', error))
    }

    render() {
        return (
            <PlacesAutocomplete
                value={this.state.address}
                onChange={this.handleChange.bind(this)}
                onSelect={this.handleSelect.bind(this)}
            >
                {({ getInputProps, suggestions, getSuggestionItemProps }) => (
                    <div className="form-container">
                        <form>
                            <input
                                {...getInputProps({
                                    placeholder: 'Search Places ...'
                                })}
                            />
                            <span className="search-button fa fa-search" />
                            <div className="autocomplete-dropdown-container">
                                {suggestions.map(suggestion => {
                                    return (
                                        <div className={"option"} {...getSuggestionItemProps(suggestion)}>
                                            <span>{suggestion.description}</span>
                                        </div>
                                    )
                                })}
                            </div>
                        </form>
                    </div>
                )}
            </PlacesAutocomplete>
        );
    }
}

export default SearchBox;
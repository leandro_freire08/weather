import React, { Component } from 'react';
import './TodayComponent.css';
import { getIconClassName } from '../../util/util';

class TodayComponent extends Component {

    render() {

        const {day, date, description, mainTemperature, weather} = this.props.data;

        const iconClass = getIconClassName(weather);

        return (
            <div className="today-component-container">
                <div className="date-container">
                    <div>{day}</div>
                    <div>{date}</div>
                </div>

                <div className="temp-container">
                    <div className="temp-text">
                        <span>{mainTemperature}</span>
                        <i className="wi wi-degrees" />
                    </div>
                </div>

                <div className="icon-desc-container">
                    <div className="icon-conatainer">
                        <i className={`wi wi-forecast-io-${weather} weather-icon ${iconClass}`} />
                    </div>
                    <div className="weather-desc">{description}</div>
                </div>
            </div>
        );
    }
}

export default TodayComponent;
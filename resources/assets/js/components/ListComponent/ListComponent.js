import React, { Component } from 'react';
import './ListComponent.css';
import { getIconClassName } from '../../util/util';

const SingleListItem = (props) => {
    const {day, date, weather, description, temperature} = props.data;

    const iconClass = getIconClassName(weather);

    return (
        <div className="single-list-item">
            <div className="li-info-container">
                <div className="li-day">
                    {day}
                </div>
                <div className="li-day">
                    {date}
                </div>
                <div className="li-temp-text">{temperature}&#x00B0;</div>
                <div className="li-desc">{description}</div>
            </div>
            <div className="li-weather-icon">
                <i className={`wi wi-forecast-io-${weather} weather-list-icon ${iconClass}`} />
            </div>
        </div>
    );
};

class ListComponent extends Component {
    render() {
        const items = this.props.data.listComponentData.map((singleDayData, key) => (<SingleListItem key={key} data={singleDayData} />));
        return (
            <div className="list-container">
                {items}
            </div>
        );
    }
}

export default ListComponent;
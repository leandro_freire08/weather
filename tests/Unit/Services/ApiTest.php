<?php

namespace Tests\Unit\Services;

use App\Helper\DateHandler;
use App\Http\Services\Api;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use function GuzzleHttp\Psr7\stream_for;
use Tests\TestCase;

class ApiTest extends TestCase
{
    /**
     * @var Api
     */
    private $api;

    public function setUp()
    {
        parent::setUp();
        $this->api = $this->mockApi();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetByDateRange()
    {
        $dateHandler = new DateHandler();
        $dateRange = $dateHandler->getDateRange(1);

        $response = $this->api->getByDateRange($dateRange);

        $this->assertArrayHasKey("today", $response);
        $this->assertArrayHasKey("past", $response);
        $this->assertArrayHasKey("forecast", $response);
    }

    private function mockApi()
    {
        $mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/json'], $this->getMockResponse()),
            new Response(200, ['Content-Type' => 'application/json'], $this->getMockResponse()),
            new Response(200, ['Content-Type' => 'application/json'], $this->getMockResponse())
        ]);

        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);
        return new Api($client);
    }

    private function getMockResponse()
    {
        return stream_for(json_encode([
            "latitude" => "37.8267",
            "longitude" => "-122.4233",
            "timezone" => "America/Los_Angeles",
            "currently" => [
                "time" => "1527633720",
                "summary" => "Clear",
                "icon" => "clear-day",
                "nearestStormDistance" => "24",
                "nearestStormBearing" => "73",
                "precipIntensity" => "0",
                "precipProbability" => "0",
                "temperature" => "70.63",
                "apparentTemperature" => "70.63",
                "dewPoint" => "53",
                "humidity" => "0.54",
                "pressure" => "1008.25",
                "windSpeed" => "8.46",
                "windGust" => "14.38",
                "windBearing" => "245",
                "cloudCover" => "0.03",
                "uvIndex" => "5",
                "visibility" => "10",
                "ozone" => "318.1"
	        ]
        ]));
    }
}

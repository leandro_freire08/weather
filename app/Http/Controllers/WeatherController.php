<?php

namespace App\Http\Controllers;

use App\Helper\DateHandler;
use App\Http\Services\Api;

class WeatherController extends Controller
{
    const RANGE_OF_DAYS = 30;

    public function index()
    {
        $dateHandler = new DateHandler();

        $dateRange = $dateHandler->getDateRange(self::RANGE_OF_DAYS);
        $client = new \GuzzleHttp\Client(['base_uri' => "https://api.darksky.net/forecast/"]);
        $api = new Api($client);

        $lat = request('lat');
        $lon = request('lon');

        $api->location($lat, $lon);

        $response = $api->getByDateRange($dateRange);
        return response()->json($response, 200);
    }
}
